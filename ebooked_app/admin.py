from django.contrib import admin
from .models.modeloUsuario import UserModel

# Register your models here.
admin.site.register(UserModel)
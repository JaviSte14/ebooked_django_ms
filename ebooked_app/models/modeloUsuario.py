from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):

    def create_user(self, idUser, password=None):
        if not idUser:
            raise ValueError('Users must have an idUser')
        user = self.model(idUser=idUser) #model se hereda de BaseUserManager
        user.set_password(password)
        user.save(using=self._db) # ORM inserta usuario en base de datos
        return user
            
    def create_superuser(self, idUser, password):

        user = self.create_user(
            idUser=idUser,
            password=password,
        ) # rehuso el primer metodo
        user.is_admin = True # Le ofrezco privilegios de admin
        user.save(using=self._db)
        return user

class UserModel(AbstractBaseUser, PermissionsMixin):

    idUser = models.BigIntegerField('idUser', null=False, primary_key=True)
    password = models.CharField('Password', max_length = 256, null=False)
    name = models.CharField('Name', max_length = 40, null=False)
    last_name = models.CharField('LastName', max_length = 40, blank= True)
    email = models.EmailField('Email', max_length = 100, unique=True, null=False)
    phone = models.BigIntegerField('Phone', blank=True, null=False)
    # se sobreescribe la función para encriptar la contraseña en la bd
    def save(self, **kwargs): # ** -> recibe un diccionario
        if not self.password:
            raise ValueError('Users must have an password')
        some_salt = 'mMUj0DrIK6vgtdIYepkIxN' # Valor secreto para cifrado
        self.password = make_password(self.password, some_salt) # Cifra la contraseña ingresada
        super().save(**kwargs)
        
    objects = UserManager()
    USERNAME_FIELD = 'idUser'
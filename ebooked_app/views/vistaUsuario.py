from rest_framework import status, views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.conf import settings
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
#----------------
from ebooked_app.serializers.serializadorUsuario import UserSerializer
from ebooked_app.models.modeloUsuario import UserModel

class UserCreateView(views.APIView):

    def post(self, request, *args, **kwargs):

        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        tokenData = {"idUser":request.data["idUser"],"password":request.data["password"]}
        tokenSerializer = TokenObtainPairSerializer(data=tokenData)
        tokenSerializer.is_valid(raise_exception=True)
        return Response(tokenSerializer.validated_data, status=status.HTTP_201_CREATED)

class UserView(views.APIView):

    permission_classes = (IsAuthenticated,)    

    def get(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False) 

        serializer = UserSerializer()
        instance = UserModel(idUser = valid_data["id_user"])
        result = serializer.to_representation(instance)

        return Response(result, status=status.HTTP_200_OK)

    def put(self, request, *args, **kwargs):

        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)

        request.data["idUser"] = valid_data["id_user"]
        instance = UserModel(idUser = valid_data["id_user"])

        serializer = UserSerializer(instance, data=request.data)        
        serializer.is_valid(raise_exception=True)        
        updatedUser = serializer.save()
        result = serializer.to_representation(updatedUser)

        return Response(result, status=status.HTTP_200_OK)
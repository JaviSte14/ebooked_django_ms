from rest_framework import serializers
from ebooked_app.models.modeloUsuario import UserModel

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserModel
        fields = ['idUser','password','name','last_name','email','phone']
    
    def create(self, validated_data):
        userInstance = UserModel.objects.create(**validated_data)
        return userInstance

    def to_representation(self, obj):
        user = UserModel.objects.get(idUser=obj.idUser)
        return {
            'idUser': user.idUser,
            'name': user.name,
            'last_name': user.last_name,
            'email': user.email,
            'phone': user.phone
        }
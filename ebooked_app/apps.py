from django.apps import AppConfig


class EbookedAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ebooked_app'
